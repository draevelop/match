# To run this project

CD to the main directory

`npm install`
callable via `run('projectsJsonUrl', 'respondentCSVFilePath')`

It also accepts an optional number for maxDistance
callable via `run('projectsJsonUrl', 'respondentCSVFilePath', 100)`

The main run function retuns a promise containing the search results.
Example usage:
```
run(projectsJsonUrl, respondentCSVFilePath).then( result => {
            console.log(result);
        });

```


The Main.ts kicks off everything and accepts a link to the projects json resource and checks for respondent csv locally.

## Tests
Run the following command from the main directory, to start the the test server
1. `npm start`
2. Open a new terminal window in the main directory
2. run `npm test`

## Notes
I really focued on accuracy of the search results and making the code reusable by programming to interfaces.


## To do
1. Mock fetch responses in main.spec.ts and remove testing dependency on the server

## Refactor Ideas
1. Use factory pattern or dependency Injection to refactor some instantiation of concrete clases.