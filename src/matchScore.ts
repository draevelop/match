import Coordinates from "./coordinates";
import IProject from "./Contracts/IProject";
import IRespondent from "./Contracts/IRespondent";
import IPoint from "./Contracts/IPoint";

export default class MatchScore {

    // Weights job title - 100, industry 50, location 20

    weights = {
        location: 30, 
        job_title: 100, 
        job_industry: 50
    }

    score : number;
    distance: number;

    constructor(respondent : IRespondent, project : IProject, maxDist : number){
        this.distance = this.getMinDistance(respondent.location(), project.locations());
        this.score = this.calculateScore(respondent, project, maxDist);
    }

    /**
     * Returns the score for a respondent and proejct. Will return -1 if outside of maxDist.
     * 
     * @param respondent The respondent interface
     * @param project the project interface
     * @param maxDist the maximum distance in km the search should consider
     */
    calculateScore(respondent : IRespondent, project : IProject, maxDist : number) : number {
        if(this.distance > maxDist){
            return -1;
        }

        let score = 0;
        score += this.jobTitleScore(respondent.jobTitle(), project.jobTitles());
        score += this.jobIndustryScore(respondent.industry(), project.industries());
        score += this.locationScore(this.distance);

        return score;

    }
    
    jobTitleScore(actualTitle : string, desiredTitles : string[]) : number{
        const lowercaseTitles = desiredTitles.map(title => title.toLowerCase());
        return lowercaseTitles.includes(actualTitle.toLowerCase()) ? this.weights.job_title : 0;
    }

    jobIndustryScore(actualIndustry: string, desiredIndustries : string[]): number {
        const lowercaseTitles = desiredIndustries.map(title => title.toLowerCase());
        return lowercaseTitles.includes(actualIndustry.toLowerCase()) ? this.weights.job_industry : 0;
    }

    locationScore(distance : number) : number{
        // Min distance to project
        const largestDiameterOnEarth = 12756;
        const score = (1 - (distance / largestDiameterOnEarth)) * this.weights.location;
        return score;
    }

    getMinDistance(p : IPoint, pointList : IPoint[]) : number {
        let minDistance = Number.MAX_SAFE_INTEGER;
        let distance;
        for(let entry of pointList){
            distance = Coordinates.distanceBetween(p, entry);
            minDistance = Math.min(minDistance, distance);
        }
        return minDistance;
    }

    getScore() : number {
        return this.score;
    }

    getDistance() : number {
        return this.distance;
    }

    getWeights() : any{
        return this.weights;
    }


    
   
}