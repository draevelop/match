import IRespondent from "./Contracts/IRespondent";
import IProject from "./Contracts/IProject";
import ISearchResult from "./Contracts/ISearchResult";
import SearchResult from "./Models/SearchResult";
import MatchScore from "./matchScore";
import { match } from "assert";

export default class Search {
    

    static getResults(respondents : IRespondent[], project : IProject, maxDist = 100) : ISearchResult[] {
        const results = [];
        for (let respondent of respondents){
            const matchScore = new MatchScore(respondent, project, maxDist);
            const distance = matchScore.getDistance();
            const score = matchScore.getScore();

            if (score >= 0){
                const name = respondent.name();
                const result = new SearchResult(name, distance, score);
                results.push(result);
            }
        }

        var sortedResults: ISearchResult[] = results.sort((res1, res2) => {
            if (res1.getScore() > res2.getScore()) {
                return 1;
            }
        
            if (res1.getScore() < res2.getScore()) {
                return -1;
            }
        
            return 0;
        });

        return results;

    }
}