import Point from "./Point"
import IProject from "../Contracts/IProject";

export default class Project implements IProject{
    
    locationList: Point[];
    jobTitleList : string[];
    industryList: string[];

    constructor(locations : Point[], jobTitles : string[] , industries : string[]){
        this.locationList = locations;
        this.jobTitleList = jobTitles;
        this.industryList = industries;
    }

    public locations() : Point[]{
        return this.locationList;
    }

    public jobTitles() : string[]{
        return this.jobTitleList;
    }

    public industries() : string[]{
        return this.industryList;
    }
}