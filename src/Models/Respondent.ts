import Point from "./Point"
import IRespondent from "../Contracts/IRespondent";

export default class Respondent implements IRespondent{
    
    loc: Point;
    job_title : string;
    work_industry: string;
    fullname: string;

    constructor(location : Point, jobTitle : string, industry : string, name : string ){
        this.loc = location;
        this.job_title = jobTitle;
        this.work_industry = industry;
        this.fullname = name;
    }

    public location() : Point{
        return this.loc;
    }

    public jobTitle() : string{
        return this.job_title;
    }

    public industry() : string{
        return this.work_industry;
    }

    public name() : string {
        return this.fullname;
    }
}