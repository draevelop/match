import ISearchResult from "../Contracts/ISearchResult"

export default class SearchResult implements ISearchResult{
    name: string;
    distance: number
    score: number

    constructor(name : string, distance : number, score : number){
        this.name = name;
        this.distance = distance;
        this.score = score;
    }

    getScore() : number {
        return this.score;
    }

    getDistance() : number {
        return this.distance;
    }

    getName() : string {
        return this.name;
    }
}