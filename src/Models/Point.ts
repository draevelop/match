import IPoint from "../Contracts/IPoint"

export default class Point implements IPoint{
    
    lat: number;
    lon: number;

    constructor(lat : number, lon : number ){
        this.lat = lat;
        this.lon = lon;
    }
}