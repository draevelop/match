import {run} from '../main';
import { expect } from 'chai';
import 'mocha';
import fetch from 'isomorphic-fetch';
import sinon from 'sinon';


describe('Search results', () => {

    beforeEach(() => {
        
        //let stub = sinon.stub(fetch).returns('hi');
        
    });

    afterEach(() => {
        sinon.restore();
    });

    it('Should return results', () => {
        //refactor to use a url instead of local file
        const respondentCSV = __dirname+'/../../server/respondents_data_test.csv';
        const projectPath = 'http://localhost:8081/project.json';

        run(projectPath, respondentCSV).then( result => {
            expect(result.length).to.equal(496);
        });

        const maxDist = 0;
        run(projectPath, respondentCSV, maxDist).then( result => {
            expect(result.length).to.equal(430);
        
        });
       
    })

});