import Coordinates from '../coordinates';
import { expect } from 'chai';
import 'mocha';

// See https://www.gpsvisualizer.com/calculators for calcuations
describe('It should calculate the distance between two coordinates', () => {
    it('should return 0 for same point', () => {
        const NY = {lat: 40.7128, lon: 74.0060}
        const distance = Coordinates.distanceBetween(NY,NY);
        expect(distance).to.equal(0);
    });

    it('should return correct distance with .05 error for large distances', () => {
        const NY = {lat: 40.7128, lon: 74.0060}
        const WY = {lat: 43.0760, lon: -107.2903}
        let realDistance = 10723.18;
        let calculatedDistance = Coordinates.distanceBetween(NY, WY);

        // 5% error is allowed
        expect(calculatedDistance).to.be.gte(realDistance * .95);
        expect(calculatedDistance).to.be.lte(realDistance * 1.05);

        const BC = {lat: 53.726669, lon: -127.647621}
        const CA = {lat: 36.7783, lon: -119.417931}
        realDistance = 1988.08;
        calculatedDistance = Coordinates.distanceBetween(BC, CA);

        // 5% error is allowed
        expect(calculatedDistance).to.be.gte(realDistance * .95);
        expect(calculatedDistance).to.be.lte(realDistance * 1.05);
    })

    it('should return correct distance small distances', () => {
        const p1 = {lat: 40.8763103, lon: -73.8635861}
        const p2 = {lat: 40.8768457, lon: -73.8647341}
        let realDistance = .99; // less than 1 km
        let calculatedDistance = Coordinates.distanceBetween(p1, p2);

        // 5% error is allowed
        expect(calculatedDistance).to.be.gte(0);
        expect(calculatedDistance).to.be.lte(realDistance);
    })

    it('should return same answer when points reversed', () => {
        const NY = {lat: 40.7128, lon: 74.0060}
        const WY = {lat: 43.0760, lon: -107.2903}
        const calculatedDistance1 = Coordinates.distanceBetween(NY, WY);
        const calculatedDistance2 = Coordinates.distanceBetween(WY, NY);
        expect(calculatedDistance1).to.equal(calculatedDistance2);
    })

});

describe('It should convert degrees to radians', () => {
    it('should return PI', () => {
        const radians = Coordinates.degreesToRadian(180);
        expect(radians).to.equal(Math.PI);
    })
})