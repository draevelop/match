import MatchScore from '../matchScore';
import { expect } from 'chai';
import 'mocha';
import Respondent from '../Models/Respondent';
import Point from '../Models/Point';
import Project from '../Models/Project';
import { match } from 'assert';

describe('It should get score for job industry', () => {

    const location = new Point(40.98, 53.59);
    const respondent = new Respondent(location, 'Software Engineer', 'communications', 'john');
    const desiredIndustries = ['travel'];
    const maxDist = 1000;

    it('should return correct score with a title match', () => {
        const desiredTitles = ['Software Engineer', 'data scientist'];
        const project = new Project([location], desiredTitles, desiredIndustries);
        const matchScore = new MatchScore(respondent, project, maxDist);
        const score = matchScore.getScore();
        const distance = matchScore.getDistance();
        const weights = matchScore.getWeights();
        expect(score).to.equal(weights.job_title + weights.location);
        
    });

    it('should return 0 with title mismatch', () => {
        const desiredTitles = ['data scientist'];
        const project = new Project([location], desiredTitles, desiredIndustries);
        const matchScore = new MatchScore(respondent, project, maxDist);
        const score = matchScore.getScore();
        const distance = matchScore.getDistance();
        const weights = matchScore.getWeights();

        expect(score).to.equal(weights.location);
    });
    

});

describe('It should get score for industry', () => {

    const location = new Point(40.98, 53.59);
    const respondent = new Respondent(location, 'Software Engineer', 'communications', 'john');
    const desiredTitles = ['data scientist'];
    
    const maxDist = 1000;

    it('should return correct score with a industry match', () => {
        const desiredIndustries = ['travel', 'Communications'];
        const project = new Project([location], desiredTitles, desiredIndustries);
        const matchScore = new MatchScore(respondent, project, maxDist);
        const score = matchScore.getScore();
        const distance = matchScore.getDistance();
        const weights = matchScore.getWeights();
        expect(score).to.equal(weights.job_industry + weights.location);
        
    });

    it('should return 0 with industry mismatch', () => {
        const desiredIndustries = ['travel'];
        const project = new Project([location], desiredTitles, desiredIndustries);
        const matchScore = new MatchScore(respondent, project, maxDist);
        const score = matchScore.getScore();
        const distance = matchScore.getDistance();
        const weights = matchScore.getWeights();
        expect(score).to.equal(weights.location);
    });
});

describe('It should get score based on location', () => {

    const location = new Point(40.98, 53.59);
    const respondent = new Respondent(location, 'Software Engineer', 'communications', 'john');
    const desiredTitles = ['data scientist'];
    const desiredIndustries = ['travel'];

    const maxDist = 1000;

    it('should return correct score for location match', () => {
        
        const project = new Project([location], desiredTitles, desiredIndustries);
        const matchScore = new MatchScore(respondent, project, maxDist);
        const score = matchScore.getScore();
        const weights = matchScore.getWeights();
        expect(score).to.equal(weights.location);

    });

    it('should return correct score for further location within range', () => {
        const desiredIndustries = ['travel'];
        const project = new Project([location], desiredTitles, desiredIndustries);

        const location2 = new Point(40.98, 54.59); //further point
        const respondent2 = new Respondent(location2, 'Software Engineer', 'communications', 'john');
        const matchScore = new MatchScore(respondent2, project, maxDist);
        
        const weights = matchScore.getWeights();
        const score = matchScore.getScore();
        
        expect(score).to.lt(weights.location);
    });

    it('should return -1 when distance out of range', () => {
        const desiredIndustries = ['travel'];
        const project = new Project([location], desiredTitles, desiredIndustries);

        const location2 = new Point(50.98, -54.59); //very far point
        const respondent2 = new Respondent(location2, 'Software Engineer', 'communications', 'john');
        const matchScore = new MatchScore(respondent2, project, maxDist);
        
        const weights = matchScore.getWeights();
        const score = matchScore.getScore();
        const distance = matchScore.getDistance();

        expect(distance).to.be.gt(maxDist);        
        expect(score).to.equal(-1);
    });
    
});


