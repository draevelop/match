import IPoint from "./IPoint";

export default interface IProject{
    locationList: IPoint[];
    jobTitleList : string[];
    industryList: string[];

    locations () : IPoint[];
    jobTitles () : string[];
    industries () : string[];
}