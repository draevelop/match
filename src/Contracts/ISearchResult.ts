export default interface ISearchResult{
    name: string;
    distance: number;
    score: number;

    getName() : string;
    getDistance() : number;
    getScore() : number;
    
    
    }