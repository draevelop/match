import IPoint from "./IPoint";

export default interface IRespondent{
    loc: IPoint;
    job_title: string;
    work_industry: string;
    fullname: string;

    location() : IPoint;
    jobTitle() : string;
    industry() : string;
    name() : string;
}