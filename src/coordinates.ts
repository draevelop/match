import IPoint from './Contracts/IPoint'
export default class Coordinates {

    static distanceBetween(p1: IPoint, p2: IPoint): number {
        // Based on https://en.wikipedia.org/wiki/Great-circle_distance
        // arctan used for highest accuracy
        const φ1 = this.degreesToRadian(p1.lat);
        const φ2 = this.degreesToRadian(p2.lat);
        const Δλ = this.degreesToRadian(p2.lon - p1.lon);

        // km
        const radius = 6371;
        const radicandTerm1 = Math.pow( Math.cos(φ2) * Math.sin(Δλ), 2);
        const radicandTerm2 = Math.pow(
            Math.cos(φ1) * Math.sin(φ2) - 
            Math.sin(φ1) * Math.cos(φ2) * Math.cos(Δλ),
         2);
        const divisor = Math.sin(φ1) * Math.sin(φ2) + 
            Math.cos(φ1) * Math.cos(φ2) * Math.cos(Δλ);
        let centralAngle = Math.atan( Math.sqrt(radicandTerm1 + radicandTerm2) / divisor);
        
        // arctan needs the angle to be between 0 and pi if negative.
        // see https://en.wikipedia.org/wiki/Talk%3AGreat-circle_distance - Problem with formula
        centralAngle = centralAngle < 0 ? centralAngle + Math.PI : centralAngle
        
        const distance = radius * centralAngle;

        return distance;
    }

    static degreesToRadian(degrees: number){
        const pi = Math.PI;
        return degrees * (pi/180);
    }
}
