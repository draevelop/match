import ISearchResult from "./Contracts/ISearchResult";
import fetch from 'isomorphic-fetch';
import 'es6-promise';
import SearchResult from "./Models/SearchResult";
import Respondent from "./Models/Respondent";
import Point from "./Models/Point";
import Project from "./Models/Project";
import MatchScore from './matchScore';

/**
 /**
  * @param projectsJsonPath The url to the projects json
  * @param respondentsCSVPath The path to the csv file
  * @param maxDist Maximum distance in kms to include in search.
  */
export async function run(projectsJsonPath : string, respondentsCSVPath : string, maxDist = 100 ) : Promise<ISearchResult[]>{

    try {
        const results = await loadResourcesPerformSearch(projectsJsonPath, respondentsCSVPath, maxDist);
        
        // Sort descending
        results.sort((obj1, obj2) => {
            if (obj1.score > obj2.score) return -1;        
            if (obj1.score < obj2.score) return 1;
            return 0;
        });

        return results;
    } catch (error) {
        return [];
    }
    
}

async function loadResourcesPerformSearch(projectsJsonPath : string, respondentsCSVPath : string, maxDist : number) {
    const project  = await loadProject(projectsJsonPath);
    let respondents = await loadRespondents(respondentsCSVPath);
    let results : ISearchResult[] = [];

    // exit if we don't get what we expect
    if(project == undefined || respondents == undefined ) return results;

    for(let respondent of respondents){
        const matchScore = new MatchScore(respondent, project, maxDist);    
        const score = matchScore.getScore();
        const distance = matchScore.getDistance();
        const name = respondent.name();
        if(score >= 0){
            results.push(new SearchResult(name, distance, score));
        }
    }
    return results;

}

async function loadProject(path : string) : Promise<Project|void>{

    let promise : Promise<Project|void> = new Promise((resolve, reject) => {

        fetch(path)
        .then(function(response : any) {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            return response.json();
        })
        .then(function(project : any) {
            const titles : string[] = project.professionalJobTitles;
            const industries : string[] = project.professionalIndustry;
            const locations : Point[] = [];

            
            for(var city of project.cities){
                locations.push(new Point(city.location.location.latitude, city.location.location.longitude));
            }

            const proj = new Project(locations, titles, industries);

            resolve(proj);
        });
    
    });

    return promise;

    // To do implement this
   
}

async function loadRespondents(path : string) : Promise<Array<Respondent>|void> {

    const fs = require('fs'); 
    const os = require('os');
    const parse = require('csv-parse');
    const respondents : Respondent[] = [];

    let promise : Promise<Array<Respondent>|void> = new Promise((resolve, reject) => {

        fs.createReadStream(path)
        .pipe(parse({ columns: true, quote: '"', ltrim: true, rtrim: true, delimiter: ',' }))
        .on('data', function(row : any) {
            const location = new Point(row.latitude, row.longitude);
            const respondent = new Respondent(location, row.jobTitle, row.industry, row.firstName);
            respondents.push(respondent);        
        })
        .on('end',function() {
            resolve(respondents);
        });
    
    });

    return promise;

}
